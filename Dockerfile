FROM python:3.9

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . .

#RUN python manage.py migrate
#CMD ["python", "manage.py", "loaddata", "accounts.json"]

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]