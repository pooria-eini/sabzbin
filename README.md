Setup:
    `docker-compose up -d`
* Run project on `localhost:8001` for api and `localhost:8002` for admin
* Super user username is Admin and password is admin for login in office
* for getting users (question number 5.a) 
    1. `localhost:8001/users`  get all users
    2. `localhost:8001/users/?o=e&c=1` get all users with just one address
    3. `localhost:8001/users/?o=gt&c=1` get all users with more than 1 address
    4. `localhost:8001/users/?o=lt&c=2` get all users with less than 1 address
    
*for adding new address  url: `localhost:8001/address`  parameters: `{user_id, title, latitude, longitude}`