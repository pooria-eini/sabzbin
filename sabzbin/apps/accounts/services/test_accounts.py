from django.test import TestCase

from .accounts import AccountServices
from ..models import User, Address


class AccountServicesTestCase(TestCase):
    fixtures = ['accounts.json']

    def test_users(self):
        first_user = User.objects.first()
        first_user_address_count = Address.objects.filter(user=first_user).count()

        self.assertEqual(AccountServices.users().first().count, first_user_address_count)

    def test_address_by_support(self):
        addresses = AccountServices.addresses_by_support()
        for ad in addresses:
            self.assertEqual(ad.create_type, Address.TYPE_SUPPORT)
