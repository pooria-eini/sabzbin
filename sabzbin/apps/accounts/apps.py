from django.apps import AppConfig
from django.db.models.signals import post_save


class AccountConfig(AppConfig):
    name = 'sabzbin.apps.accounts'

    def ready(self):
        from .models import Address
        from .signals import send_notification
        post_save.connect(receiver=send_notification, sender=Address)
