from django.db.models import Count
from ..models import User, Address


class AccountServices:
    @staticmethod
    def addresses_by_support():
        return Address.objects.filter(create_type=Address.TYPE_SUPPORT)

    @staticmethod
    def users():
        return User.objects.all().annotate(count=Count('addresses'))
