import uuid

from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    id = models.UUIDField(_("Unique User ID"), default=uuid.uuid4, primary_key=True)
    mobile_number = models.CharField(_("Mobile Number"), max_length=15, null=False, blank=False, )
    created = models.DateTimeField(_("Created date time"), auto_now_add=True, )
    updated = models.DateTimeField(_("Updated date time"), auto_now=True, )

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    def __str__(self):
        return f'{self.username}'


class Address(models.Model):
    TYPE_CLIENT = "CL"
    TYPE_SUPPORT = "SU"
    TYPE_CHOICES = (
        (TYPE_CLIENT, _("Client")),
        (TYPE_SUPPORT, _("Support")),
    )

    id = models.UUIDField(_("Unique Address ID"), default=uuid.uuid4, primary_key=True)
    title = models.CharField(_("Title"), max_length=32, null=False, blank=False, )
    latitude = models.FloatField(_("Lat"))
    longitude = models.FloatField(_("Long"))
    create_type = models.CharField(_("Created Type"), max_length=2, choices=TYPE_CHOICES, )
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='addresses', null=False, )
    created = models.DateTimeField(_("Created date time"), auto_now_add=True, )
    updated = models.DateTimeField(_("Updated date time"), auto_now=True, )

    class Meta:
        verbose_name = _("Address")
        verbose_name_plural = _("Addresses")

    def __str__(self):
        return f'{str(self.user)} --> {self.title}'
