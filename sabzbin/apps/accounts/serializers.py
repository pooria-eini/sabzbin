from rest_framework import serializers

from .models import User, Address


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name']


class UserSerializerForSupport(serializers.ModelSerializer):
    count = serializers.IntegerField()

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'count']


class CreateAddressSerializer(serializers.ModelSerializer):
    user_id = serializers.UUIDField()

    class Meta:
        model = Address
        fields = ['user_id', 'title', 'latitude', 'longitude']
