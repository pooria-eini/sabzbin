from django.test import TestCase
from rest_framework.test import APIRequestFactory

from .api import UsersListAPIView
from .services import AccountServices


class UserListAPIViewTestCase(TestCase):
    fixtures = ['accounts.json']
    USERS_URL = '/users/'

    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.user_api = UsersListAPIView()

    def test_get_queryset(self):
        request = self.factory.get(self.USERS_URL)
        self.user_api.request = request
        queryset = self.user_api.get_queryset()
        self.assertEqual(queryset.count(), AccountServices.users().count())

    def test_get_queryset_gt(self):
        request = self.factory.get(self.USERS_URL, {'o': 'gt', 'c': '1'})
        self.user_api.request = request
        queryset = self.user_api.get_queryset()
        expected = AccountServices.users().filter(count__gt=1)
        self.assertEqual(queryset.count(), expected.count())
        for q in queryset:
            self.assertGreater(q.count, 1)

    def test_get_queryset_lt(self):
        request = self.factory.get(self.USERS_URL, {'o': 'lt', 'c': '2'})
        self.user_api.request = request
        queryset = self.user_api.get_queryset()
        expected = AccountServices.users().filter(count__lt=2)
        self.assertEqual(queryset.count(), expected.count())
        for q in queryset:
            self.assertLess(q.count, 2)

    def test_get_queryset_e(self):
        request = self.factory.get(self.USERS_URL, {'o': 'e', 'c': '2'})
        self.user_api.request = request
        queryset = self.user_api.get_queryset()
        expected = AccountServices.users().filter(count=2)
        self.assertEqual(queryset.count(), expected.count())
        for q in queryset:
            self.assertEqual(q.count, 2)
