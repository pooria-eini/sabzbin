from rest_framework.generics import ListAPIView, CreateAPIView

from .services import AccountServices
from .serializers import UserSerializerForSupport, CreateAddressSerializer


class UsersListAPIView(ListAPIView):
    queryset = AccountServices.users()
    serializer_class = UserSerializerForSupport

    def get_queryset(self):
        op = self.request.GET.get('o', None)
        c = self.request.GET.get('c', 0)
        queryset = super(UsersListAPIView, self).get_queryset()
        if op in ["gt", "lt"]:
            return queryset.filter(**{f'count__{op}': c})
        elif op == "e":
            return queryset.filter(count=c)
        return queryset


class CreateAddressAPIView(CreateAPIView):
    serializer_class = CreateAddressSerializer
